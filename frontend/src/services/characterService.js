import axios from 'axios';

const BASE_URL = 'http://localhost:5000/characters/';

export default {
  async createCharacter(characterName) {
    try {
      const response = await axios.post(BASE_URL + "create", { name: characterName });
      if (response && response.data) {
        const characterData = response.data;
        console.log('Personnage créé avec succès:', characterData);
        return characterData;
      }
    } catch (error) {
      console.error('Erreur lors de la création du personnage:', error);
      throw error;
    }
  },
  async getCharacter(characterId) {
    try {
      const response = await axios.post(BASE_URL + "character-infos", {id : characterId});
      if(response && response.data){
        const characterData = response.data;
        return characterData
      }
    }catch(error){
      console.error('Erreur lors de la création du personnage:', error);
      throw error;
    }
  },
  async damageCharacter(id,damage){
    try {
      const response = await axios.post(BASE_URL + "damage", {id: id, damage : damage});
      if(response && response.data){
        const characterData = response.data;
        return characterData
      }
    }catch(error){
      console.error('Erreur lors de la création du personnage:', error);
      throw error;
    }
  },
  async potionFound(characterId){
    try{
      const response = await axios.post(BASE_URL + "potion-found", {id : characterId});
      if (response && response.data) {
        const message = response.data;
        console.log(message);
        return message;
      }
    }catch(error){
      console.error('Erreur lors de la création du personnage:', error);
      throw error;
    }
  },
  async healPlayer(characterId){
    try{
      const response = await axios.post(BASE_URL + "heal", {id : characterId});
      if (response && response.data) {
        const message = response.data;
        console.log(message);
        return message;
      }
    }catch(error){
      console.error('Erreur lors de la création du personnage:', error);
      throw error;
    }
  },
  async objectivesAchieve(characterId){
    try{
      const response = await axios.post(BASE_URL + "objectivs", {id : characterId});
      if (response && response.data) {
        const message = response.data;
        console.log(message);
        return message;
      }
    }catch(error){
      console.error('Erreur lors de la création du personnage:', error);
      throw error;
    }
  }
};
