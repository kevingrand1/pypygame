import axios from 'axios';

const BASE_URL = 'http://localhost:5000/enemies/';

export default {
  async generateEnemies() {
    try {
      const response = await axios.get(BASE_URL + "generate");
      if (response && response.data) {
        const enemies = response.data;
        console.log('Enemies créé avec succès:', enemies);
        return enemies;
      }
    } catch (error) {
      console.error('Erreur lors de la création du personnage:', error);
      throw error;
    }
  },
};
