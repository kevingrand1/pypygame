import axios from 'axios';

const BASE_URL = 'http://localhost:5000/events/';

export default {
  async getRandomEvent() {
    try {
      const response = await axios.get(BASE_URL + "random-event");
      if (response && response.data) {
        const events = response.data;
        console.log('Events générés:', events);
        return events;
      }
    } catch (error) {
      console.error('Erreur lors de la création du personnage:', error);
      throw error;
    }
  },
  async getRandomDamage(){
    try {
      const response = await axios.get(BASE_URL + "perte-hp");
      if (response && response.data) {
        const hp = response.data;
        console.log('HP à déduire:', hp);
        return hp;
      }
    } catch (error) {
      console.error('Erreur lors de la création du personnage:', error);
      throw error;
    }
  }
};
