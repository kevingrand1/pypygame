import Vue from 'vue';
import VueRouter from 'vue-router';
import StartGame from './views/StartGame.vue';
import CharacterDetails from './views/CharacterDetails.vue';
import EventPage from './views/EventPage.vue';
import EndGame from './views/EndGame.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'StartGame',
    component: StartGame,
  },
  {
    path: '/character-infos',
    name: 'CharacterDetails',
    component: CharacterDetails,
  },
  {
    path: '/event-display',
    name: 'EventPage',
    component: EventPage,
  },
  {
    path: '/endGame/:condition',
    component: EndGame,
    props: (route) => ({ condition: route.params.condition })
  }
];

const router = new VueRouter({
  mode: 'history',
  routes,
});

export default router;
