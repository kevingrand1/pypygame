
# PyPyGame
Un petit jeu développé en python, vuejs utilisant du sql pour la persistance

## Installation des dépendances

À la racine du projet:
```bash
pip install -r requirements.txt
```

Dans le dossier /frontend:
```bash
npm i
```

## Instanciation de la base de donnée

Permet de créer et initialiser la base de donnée en fonction des models.
Commande à exécuter dans le dossier /backend
```bash
flask db init
flask db migrate
flask db upgrade
```

Une fois la base de donnée créée nous utilisons un populator afin d'ajouter certaines entrées aléatoirement.
Le fichier se trouve à la racine du projet
```bash
python3 database_populator.py
```

## Lancement de l'application

Pour lancer le front (/frontend):
```bash
npm run serve 
```

Pour lancer le back (/backend):
```bash
flask run
```
## La partie tests

Pour lancer les tests partie back vous devez vous trouver à la racine du projet:
```bash
python3 -m unittest run_tests.py -v
```

Pour lancer les tests partie selenium (front) vous devez aller à la racine du projet puis /selenium-tests:
```bash
python3 test_gameplay.py
```
## Jenkins
 commande pour lancer jenkins
 ```bash
   brew services start jenkins-lts
  ```

