from backend.app import app, db
from backend.models.Character import Character
from backend.models.Event import Event
from backend.models.Enemy import Enemy
from faker import Faker
import random

def generate_characters(num_characters):
    fake = Faker()
    characters = []
    for _ in range(num_characters):
        name = fake.name()
        health = random.randint(50, 100)
        attack = random.randint(5, 15)
        defense = random.randint(1, 10)
        character = Character(name=name, health=health, attack=attack, defense=defense)
        characters.append(character)
    return characters

def generate_events(num_events):
    fake = Faker()
    event_types = ["bataille", "perte_hp", "gain_potion"]
    events = []
    for _ in range(num_events):
        name = fake.word()
        description = fake.sentence()
        is_positive = fake.boolean()
        event_type = random.choice(event_types)
        event = Event(name=name, description=description, is_positive=is_positive, event_type=event_type)
        events.append(event)
    return events

def generate_enemies(num_enemies):
    fake = Faker()
    enemies = []
    for _ in range(num_enemies):
        name = fake.name()
        strength = random.randint(1, 10)
        health = random.randint(50, 100)
        enemy = Enemy(name=name, strength=strength, health=health)
        enemies.append(enemy)
    return enemies

def populate_database(num_characters, num_events, num_enemies):
    with app.app_context():
        characters = generate_characters(num_characters)
        events = generate_events(num_events)
        enemies = generate_enemies(num_enemies)
        db.session.bulk_save_objects(characters)
        db.session.bulk_save_objects(events)
        db.session.bulk_save_objects(enemies)
        db.session.commit()

if __name__ == '__main__':
    populate_database(10, 20, 5)
