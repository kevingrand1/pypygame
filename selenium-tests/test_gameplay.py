import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import random
import string
import time

class WelcomePage:

    title = "frontend"
    header_title = "Start Game"
    url = "http://localhost:8080"

    def __init__(self, driver):
        self.driver = driver
        self.query_field = WelcomePage.QueryField(driver)

    def go(self):
        print("going to : ", WelcomePage.url)
        self.driver.get(WelcomePage.url)
        return self

    def get_title(self):
        title = self.driver.title
        print("title : ", title)
        return title

    def get_header_title(self):
        header_title_element = self.driver.find_element(By.CSS_SELECTOR, "h1")
        header_title = header_title_element.text
        print("Header title: ", header_title)
        return header_title
    
    def click_start_game(self):
        start_button = self.driver.find_element(By.XPATH, "//button[text()='Start the Game']")
        start_button.click()
        if(start_button):
            return True
        return False
    def getForm(self):
        return self.driver.find_element(By.CSS_SELECTOR, "form")

    def fillForm(self):
        random_name = ''.join(random.choices(string.ascii_letters, k=10))

        character_name_input = self.driver.find_element(By.CSS_SELECTOR, "#characterName")
        character_name_input.clear()
        character_name_input.send_keys(random_name)

        create_character_button = self.driver.find_element(By.CSS_SELECTOR, "form button[type='submit']")
        create_character_button.click()
        #On check qu'on est bien sur une nouvelle page
        return self.driver.current_url

    class QueryField:

        def __init__(self, driver):
            self.driver = driver

        def __get_elem(self):
            if not hasattr(self, 'elem'):
                self.elem = self.driver.find_element(By.CSS_SELECTOR, "[name='q']")
            return self.elem

        def clear(self):
            self.__get_elem().clear()
            return self

        def search(self, query):
            input = self.__get_elem()
            input.send_keys("pycon")
            input.send_keys(Keys.RETURN)
            return self

class Characters_info(WelcomePage):
    def __init__(self, driver):
        super().__init__(driver)

    #On check si au moins un bouton d'event existe
    def getButtonsLen(self):
        element_div = self.driver.find_element(By.CSS_SELECTOR, "div")
        button_divs = element_div.find_elements(By.CSS_SELECTOR, "div button")
        return len(button_divs)
    def clickButton(self):
        element_div = self.driver.find_element(By.CSS_SELECTOR, "div")
        button_divs = element_div.find_elements(By.CSS_SELECTOR, "div button")
        first_button = button_divs[0]
        first_button.click()
        if(first_button):
            return True
        return False
   

class PythonOrgSearch(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox()

    def tearDown(self):
        pass
        self.driver.close()

    def test_accueil(self):
        page = WelcomePage(self.driver).go()
        assert page.get_title() == WelcomePage.title
        assert page.get_header_title()==WelcomePage.header_title
        assert page.click_start_game() == True
        assert page.getForm().is_displayed()
        assert page.fillForm() == "http://localhost:8080/character-infos"

    def test_characters_infos(self):
        #On re check tout sauf qu'on va aller plus loin
        page = Characters_info(self.driver).go()
        assert page.get_title() == Characters_info.title
        assert page.get_header_title()==Characters_info.header_title
        assert page.click_start_game() == True
        assert page.getForm().is_displayed()
        assert page.fillForm() == "http://localhost:8080/character-infos"

        #Récupération des infos de la page joueur
        element_div = self.driver.find_element(By.CSS_SELECTOR, "div")

        expected_values = {
            "Health:": "100",
            "Attack:": "10",
            "Defense:": "5",
            "Potions:": "3",
            "Objectivs:": "10"
        }
        for key, value in expected_values.items():
            assert key in element_div.text
            assert value in element_div.text
        assert page.getButtonsLen() > 0

    def test_events(self):
        page = Characters_info(self.driver).go()
        assert page.get_title() == Characters_info.title
        assert page.get_header_title()==Characters_info.header_title
        assert page.click_start_game() == True
        assert page.getForm().is_displayed()
        assert page.fillForm() == "http://localhost:8080/character-infos"
        #assert page.clickButton() == True    

        buttons = self.driver.find_elements(By.XPATH, "//button[contains(text(), 'bataille')]")
        button_element = next((button for button in buttons if button.is_displayed()), None)

        # Vérifier si le bouton a été trouvé
        if button_element:
            # Clique sur le bouton
            button_element.click()
            
            # Attendre un court instant pour permettre à la page de se mettre à jour
            time.sleep(1)

            attack_element = self.driver.find_element(By.CSS_SELECTOR, "div > div p:nth-child(3)")
            attack_value = attack_element.text.strip()
            index = attack_element.text.split(':')
            attackValue = int(index[1])
            print("Attaque : {}".format(attackValue))
            # Récupère la première div avec la deuxième valeur

           


            second_value_element = self.driver.find_element(By.XPATH, "//div[@id='app']//div[2]//div[2]//p[3]")
            second_value = second_value_element.text.strip()
            print("vie monstre1 : {}".format(second_value))


            # Clique sur le bouton "Attaquer"
            # attack_button = second_value_element[0].find_element(By.CSS_SELECTOR, "button")
            wait = WebDriverWait(self.driver, 10)
            button = wait.until(EC.visibility_of_element_located((By.XPATH, "//button[contains(text(), 'Attaquer')]")))

            button.click()

            # Attendre un certain temps pour que la valeur soit mise à jour
            time.sleep(2)

            # Récupère à nouveau la première div avec la nouvelle valeur

            updated_value_element = self.driver.find_element(By.XPATH, "//div[@id='app']//div[2]//div[2]//p[3]")
            updated_value = updated_value_element.text.strip()
            print("vie monstre2 : {}".format(updated_value))

            #rajouter les tests sur les dégats (désolé monsieur y'a un ptit bug qu'on a pas corrigé)
            # degats = second_value - updated_value
            # assert attack_value == degats
            assert True
        else:
            print("Le bouton avec le texte 'bataille' n'a pas été trouvé.")

if __name__ == "__main__":
    unittest.main()