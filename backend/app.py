from flask import Flask, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
from flask_migrate import Migrate

app = Flask(__name__)
cors = CORS(app, origins='*')
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///db.db'
db = SQLAlchemy(app)
migrate = Migrate(app, db)

@app.route('/status', methods=['GET'])
def db_status():
    try:
        db.engine.connect()
        return jsonify({'status': 'database connected'})
    except Exception as e:
        return jsonify({'status': 'database disconnected', 'error': str(e)})
