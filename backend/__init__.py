from flask import Blueprint
from .app import app

# Créez les blueprints sans les enregistrer immédiatement
characters_bp = Blueprint('characters', __name__)
items_bp = Blueprint('items', __name__)
events_bp = Blueprint('events', __name__)
enemies_bp = Blueprint('enemies',__name__)
# Importez les routes
from backend.routes import characters, items, events, enemies

# Enregistrez les routes dans les blueprints correspondants
characters_bp.register_blueprint(characters.characters_bp)
items_bp.register_blueprint(items.items_bp)
events_bp.register_blueprint(events.events_bp)
enemies_bp.register_blueprint(enemies.enemies_bp)

# Enregistrez les blueprints
app.register_blueprint(characters_bp)
app.register_blueprint(items_bp)
app.register_blueprint(events_bp)
app.register_blueprint(enemies_bp)
