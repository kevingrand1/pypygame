import unittest
from flask import current_app
from backend.models.Event import Event
from backend.app import app, db


class EventsTestCase(unittest.TestCase):

    def setUp(self):
        self.app = app.test_client()
        self.app_context = app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_get_events(self):
        event1 = Event(name='Event 1', is_positive=True)
        event2 = Event(name='Event 2', is_positive=False)
        db.session.add(event1)
        db.session.add(event2)
        db.session.commit()

        response = self.app.get('/events')
        data = response.get_json()

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(data), 2)

    def test_create_event(self):
        response = self.app.post('/events', json={'name': 'New Event'})
        data = response.get_json()

        self.assertEqual(response.status_code, 200)
        self.assertEqual(data['message'], 'Event created successfully')


if __name__ == '__main__':
    unittest.main()
