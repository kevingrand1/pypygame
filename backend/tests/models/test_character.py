import unittest
from backend.models.Character import Character
from flask import current_app
from backend.app import app, db


class CharactersTestCase(unittest.TestCase):

    def setUp(self):
        self.app = app.test_client()
        self.app_context = app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_get_characters(self):
        character1 = Character(name='Character 1')
        character2 = Character(name='Character 2')
        db.session.add(character1)
        db.session.add(character2)
        db.session.commit()

        response = self.app.get('/characters')
        data = response.get_json()

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(data), 2)

    def test_create_character(self):
        response = self.app.post('/characters/create', json={'name': 'New Character'})
        data = response.get_json()

        self.assertEqual(response.status_code, 200)
        self.assertEqual(data['message'], 'Character created successfully')


if __name__ == '__main__':
    unittest.main()
