import unittest
from flask import current_app
from backend.models.Enemy import Enemy
from backend.app import app, db


class EnemiesTestCase(unittest.TestCase):

    def setUp(self):
        self.app = app.test_client()
        self.app_context = app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_get_enemies(self):
        enemy1 = Enemy(name='Enemy 1', strength=10, health=100)
        enemy2 = Enemy(name='Enemy 2', strength=15, health=150)
        db.session.add(enemy1)
        db.session.add(enemy2)
        db.session.commit()

        response = self.app.get('/enemies')
        data = response.get_json()

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(data), 2)

    # def test_get_enemy(self):
    #     enemy = Enemy(name='Enemy 1', strength=10, health=100)
    #     db.session.add(enemy)
    #     db.session.commit()

    #     response = self.app.get(f'/enemies/{enemy.id}')
    #     data = response.get_json()

    #     self.assertEqual(response.status_code, 200)
    #     self.assertEqual(data['name'], 'Enemy 1')

    # def test_get_nonexistent_enemy(self):
    #     response = self.app.get('/enemies/999')
    #     data = response.get_json()

    #     self.assertEqual(response.status_code, 404)
    #     self.assertEqual(data['message'], 'Enemy not found')


if __name__ == '__main__':
    unittest.main()
