from unittest import TestCase
from backend.models.Item import Item
from backend.app import app, db

class RoutesItemsTestCase(TestCase):
    def setUp(self):
        app.testing = True
        self.app = app.test_client()
        self.app_context = app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_get_items(self):
        item1 = Item(name='item 1')
        item2 = Item(name='item 2')
        db.session.add(item1)
        db.session.add(item2)
        db.session.commit()

        response = self.app.get('/items')
        data = response.get_json()

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(data), 2)

    def test_create_item(self):
        response = self.app.post('/items/create', json={'name': 'New Item'})
        data = response.get_json()

        self.assertEqual(response.status_code, 200)
        self.assertEqual(data['message'], 'Item created successfully')

if __name__ == '__main__':
    unittest.main()