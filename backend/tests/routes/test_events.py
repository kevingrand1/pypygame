import unittest
from unittest.mock import patch
from flask import current_app
from backend.app import app, db
from backend.models.Event import Event
from backend.routes.events import events_bp


class RoutesEventsTestCase(unittest.TestCase):

    def setUp(self):
        app.testing = True
        self.app = app.test_client()
        self.app_context = app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def get_random_events(self):
        return ['event1', 'event2', 'event3']

    def test_get_events_route(self):
        event1 = Event(name='Event 1')
        event2 = Event(name='Event 2')
        db.session.add(event1)
        db.session.add(event2)
        db.session.commit()

        response = self.app.get('/events')
        data = response.get_json()

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(data), 2)

    def test_create_event_route(self):
        response = self.app.post('/events', json={'name': 'New Event'})
        data = response.get_json()

        self.assertEqual(response.status_code, 200)
        self.assertEqual(data['message'], 'Event created successfully')

    def test_get_random_event_route_with_events(self):
            with patch('backend.routes.events.get_random_events', self.get_random_events):
                response = self.app.get('/events/random-event')
                data = response.get_json()
                self.assertEqual(response.status_code, 200)
                self.assertIsInstance(data, list)

    def test_get_random_event_route_without_events(self):
        with patch('backend.routes.events.get_random_events', return_value=None):
            response = self.app.get('/events/random-event')
            data = response.get_json()
            self.assertEqual(response.status_code, 404)
            self.assertEqual(data['message'], 'No random events available')


# TODO: test de la route 'events/perte-hp' a réimplémenter
#     def test_perte_hp_route(self):
#         response = self.app.get('/events/perte-hp')
#         json_data = response.get_json()
#         self.assertEqual(response.status_code, 200)
#         self.assertEqual(json_data['message'], 'HP loss successful')



if __name__ == '__main__':
    unittest.main()
