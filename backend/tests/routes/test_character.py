import unittest
from flask import current_app
from flask_testing import TestCase
from backend.app import app, db
from backend.models.Character import Character
from backend.routes.characters import characters_bp


class RoutesCharactersTestCase(TestCase):

    def create_app(self):
        app.config['TESTING'] = True
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///:memory:'
        return app

    def setUp(self):
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_get_characters_route(self):
        character1 = Character(name='Character 1')
        character2 = Character(name='Character 2')
        db.session.add(character1)
        db.session.add(character2)
        db.session.commit()

        response = self.client.get('/characters')
        data = response.json

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(data), 2)

    def test_create_character_route(self):
        response = self.client.post('/characters/create', json={'name': 'New Character'})
        data = response.json

        self.assertEqual(response.status_code, 200)
        self.assertEqual(data['message'], 'Character created successfully')


if __name__ == '__main__':
    unittest.main()
