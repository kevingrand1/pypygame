from unittest import TestCase
from unittest.mock import patch
from backend.controllers.characters_controller import get_characters, create_character
from backend.models.Character import Character
from backend.app import app, db

class ControllerCharactersTestCase(TestCase):
    def setUp(self):
        app.testing = True
        self.app = app.test_client()
        self.app_context = app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_get_characters(self):
        # Créer des personnages fictifs pour le test
        character1 = Character(name='Character 1', health=100, attack=10, defense=5)
        character2 = Character(name='Character 2', health=150, attack=15, defense=8)
        db.session.add(character1)
        db.session.add(character2)
        db.session.commit()

        response = self.app.get('/characters')
        self.assertEqual(response.status_code, 200)
        characters = response.get_json()
        self.assertIsInstance(characters, list)
        self.assertEqual(len(characters), 2)
        self.assertEqual(characters[0]['name'], 'Character 1')
        self.assertEqual(characters[1]['name'], 'Character 2')

    def test_create_character(self):
        with patch('backend.controllers.characters_controller.create_character', return_value={'message': 'Character created successfully', 'id': 1}):
            response = self.app.post('/characters/create', json={'name': 'New Character'})
            self.assertEqual(response.status_code, 200)
            data = response.get_json()
            self.assertEqual(data['message'], 'Character created successfully')
            self.assertEqual(data['id'], 1)
