import unittest
from unittest import mock
from unittest.mock import patch
from flask import Flask
from backend.controllers.enemies_controller import get_enemies, create_enemy, generate_enemies
from backend.models.Enemy import Enemy
from backend.app import app, db

class ControllerEnemiesTestCase(unittest.TestCase):
    def setUp(self):
        app.testing = True
        self.app = app.test_client()
        self.app_context = app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_get_enemies(self):
        enemy1 = Enemy(name='Enemy 1', strength=20, health=100)
        enemy2 = Enemy(name='Enemy 2', strength=15, health=80)
        db.session.add_all([enemy1, enemy2])
        db.session.commit()
        response = self.app.get('/enemies')
        self.assertEqual(response.status_code, 200)
        data = response.get_json()
        self.assertIsInstance(data, list)
        self.assertEqual(len(data), 2)
        self.assertDictEqual(data[0], {'id': 1, 'name': 'Enemy 1', 'strength': 20, 'health': 100})
        self.assertDictEqual(data[1], {'id': 2, 'name': 'Enemy 2', 'strength': 15, 'health': 80})

# TODO: reimplementer ce test

#     @mock.patch('backend.controllers.enemies_controller.random.choice')
#     @mock.patch('backend.controllers.enemies_controller.Enemy.query')
#     def test_generate_enemies(self, mock_query, mock_choice):
#        enemies = [
#            {'id': 1, 'name': 'Enemy1', 'strength': 5, 'health': 10},
#            {'id': 2, 'name': 'Enemy2', 'strength': 7, 'health': 12},
#            {'id': 3, 'name': 'Enemy3', 'strength': 3, 'health': 8}
#        ]
#
#        mock_query.all.return_value = enemies
#        mock_choice.side_effect = [enemies[0], enemies[1]]
#        result = generate_enemies()
#        self.assertEqual(len(result), 2)
#        self.assertEqual(result[0], enemies[0])
#        self.assertEqual(result[1], enemies[1])
#        mock_query.all.assert_called_once()
#        mock_choice.assert_called_with(enemies)


if __name__ == '__main__':
    unittest.main()
