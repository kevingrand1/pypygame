from ..app import db

class Event(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), nullable=False)
    description = db.Column(db.Text)
    is_positive = db.Column(db.Boolean, default=True)
    event_type = db.Column(db.String(50))

