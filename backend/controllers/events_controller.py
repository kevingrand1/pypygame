from flask import jsonify
from ..models.Event import Event
from ..app import db
import random

def get_events():
    events = Event.query.all()
    result = []
    for event in events:
        result.append({
            'id': event.id,
            'name': event.name,
            'description': event.description,
            'is_positive': event.is_positive
        })
    return jsonify(result)

def create_event(name):
    event = Event(name=name)
    
    db.session.add(event)
    db.session.commit()
    return {'message': 'Event created successfully'}

def get_random_events():
    events = Event.query.all()
    result = []
    for event in events:
        result.append({
            'id': event.id,
            'name': event.name,
            'description': event.description,
            'is_positive': event.is_positive,
            'event_type' : event.event_type
        })
    random_events = []
    for i in range(1, random.randint(2,5)):
        random_events.append(random.choice(result))
    return random_events

def perte_hp():
    return str(random.randint(1,10))