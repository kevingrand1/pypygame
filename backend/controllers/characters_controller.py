from flask import Blueprint, jsonify, request
from ..models.Character import Character 
from ..app import db

def get_characters():
    characters = Character.query.all()
    result = []
    for character in characters:
        result.append({
            'id': character.id,
            'name': character.name,
            'health': character.health,
            'attack': character.attack,
            'defense': character.defense,
            'potions': character.potions,
            'objectifs' : character.objectifs
        })
    return jsonify(result)

def create_character(name):
    character = Character(name=name)
    db.session.add(character)
    db.session.commit()
    return {'message': 'Character created successfully', 'id': character.id}


def damage_character(id, damage):
    character = Character.query.get(id)
    if character:
        # Appliquer les dommages au personnage
        character.health -= damage
        db.session.commit()
        return {'message': 'Character damaged successfully', 'id':character.id}
    else:
        return {'message': 'Character not found'}

def add_player_potion(id):
    character = Character.query.get(id)
    if character:
        # Appliquer les dommages au personnage
        character.potions += 1
        db.session.commit()
        return {'message': 'Character potions added successfully', 'id':character.id}
    else:
        return {'message': 'Character not found'}

def heal_player(id):
    character = Character.query.get(id)
    if character:
        # Appliquer les dommages au personnage
        if (character.potions > 0):
            character.health += 10
            character.potions -= 1
        db.session.commit()
        return {'message': 'Character potions used successfully', 'character_health':character.health, 'character_potions':character.potions }
    else:
        return {'message': 'Character not found'}
def objectivs(id):
    character = Character.query.get(id)
    if character:
        # Appliquer les dommages au personnage
        if (character.objectifs > 0):
            character.objectifs -= 1
        db.session.commit()
        return {'message': 'Character potions used successfully', 'character_objectifs':character.objectifs}
    else:
        return {'message': 'Character not found'}