from flask import jsonify
from ..models.Item import Item
from ..app import db

def get_items():
    items = db.session.query(Item).all()
    result = []
    for item in items:
        result.append({
            'id': item.id,
            'name': item.name,
            'description': item.description
        })
    return jsonify(result)

def create_item(name):
    item = Item(name=name)
    db.session.add(item)
    db.session.commit()
    return {'message': 'Item created successfully'}
