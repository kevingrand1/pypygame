from flask import jsonify, request
from ..models.Enemy import Enemy
from ..app import db
import random

def get_enemies():
    enemies = Enemy.query.all()
    result = []
    for enemy in enemies:
        result.append({
            'id': enemy.id,
            'name': enemy.name,
            'strength': enemy.strength,
            'health': enemy.health
        })
    return jsonify(result)


def create_enemy(name):
    enemy = Enemy(name=name)
    db.session.add(enemy)
    db.session.commit()
    return {'message': 'Enemy created successfully'}

def generate_enemies():
    enemies = Enemy.query.all()
    result = []
    for enemy in enemies:
        result.append({
            'id': enemy.id,
            'name': enemy.name,
            'strength': enemy.strength,
            'health': enemy.health
        })
    rdenemies = []
    for i in range(1,random.randint(2,4)):
        rdenemies.append(random.choice(result))
    return rdenemies