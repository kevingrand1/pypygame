from flask import Blueprint, jsonify, request
from ..controllers.characters_controller import get_characters, create_character, damage_character, add_player_potion, heal_player, objectivs
from ..models.Character import Character

characters_bp = Blueprint('characters', __name__)

@characters_bp.route('/test', methods=['GET'])
def test():
    return jsonify({'message': 'test'})

@characters_bp.route('/characters', methods=['GET'])
def get_characters_route():
    characters = Character.query.all()
    result = []
    for character in characters:
        result.append({
            'id': character.id,
            'name': character.name,
            'health': character.health,
            'attack': character.attack,
            'defense': character.defense,
            'potions': character.potions,
            'objectifs' : character.objectifs
            # Ajoutez ici d'autres attributs du personnage
        })
    return jsonify(result)

@characters_bp.route('/characters/character-infos', methods=['POST'])
def get_characters():
    data = request.get_json()
    character_id = data.get('id')
    try:
        character = Character.query.get(character_id)
        if character:
            character_data = {
                'id': character.id,
                'name': character.name,
                'health': character.health,
                'attack': character.attack,
                'defense': character.defense,
                'potions': character.potions,
                'objectifs' : character.objectifs
            }
            return jsonify(character_data)
        else:
            return jsonify({'message': 'Character not found'}), 404
    except Exception as e:
        return jsonify({'status': 'error', 'error': str(e)})

@characters_bp.route('/characters/create', methods=['POST'])
def create_character_route():
    data = request.get_json()
    name = data.get('name')
    return create_character(name)

@characters_bp.route('/characters/damage', methods=['POST'])
def damage_character_route():
    data = request.get_json()
    id = data.get('id')
    damage = data.get('damage')
    return damage_character(id,damage)

@characters_bp.route('/characters/potion-found', methods=['POST'])
def potion_found_route():
    data = request.get_json()
    id_character = data.get('id')
    return add_player_potion(id_character)

@characters_bp.route('/characters/heal', methods=['POST'])
def heal_route():
    data = request.get_json()
    id_character = data.get('id')
    return heal_player(id_character)

@characters_bp.route('/characters/objectivs', methods=['POST'])
def objectivs_route():
    data = request.get_json()
    id_character = data.get('id')
    return objectivs(id_character)