from flask import Blueprint, request, jsonify
from ..controllers.events_controller import get_events, create_event, get_random_events, perte_hp

events_bp = Blueprint('events', __name__)


@events_bp.route('/events', methods=['GET'])
def get_events_route():
    return get_events()


@events_bp.route('/events', methods=['POST'])
def create_event_route():
    data = request.get_json()
    name = data.get('name')
    return create_event(name)


@events_bp.route('/events/random-event', methods=['GET'])
def get_random_event_route():
    events = get_random_events()
    if events:
        return jsonify(events)
    else:
        return jsonify({'message': 'No random events available'}), 404


@events_bp.route('/events/perte-hp', methods=['GET'])
def perte_hp_route():
    response = perte_hp()
    if response:
        return {'message': 'HP loss successful'}, jsonify(response)
    else:
        return jsonify({'message': 'Failed to perform HP loss'}), 500
