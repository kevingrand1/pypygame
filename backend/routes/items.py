from flask import Blueprint ,request
from ..controllers.items_controller import get_items, create_item

items_bp = Blueprint('items', __name__)


@items_bp.route('/items', methods=['GET'])
def get_items_route():
    return get_items()


@items_bp.route('/items/create', methods=['POST'])
def create_item_route():
    data = request.get_json()
    name = data.get('name')
    return create_item(name)
