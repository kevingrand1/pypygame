from flask import Blueprint, jsonify
from ..controllers.enemies_controller import get_enemies, create_enemy, generate_enemies
from ..models.Enemy import Enemy
from ..app import db

enemies_bp = Blueprint('enemies', __name__)


@enemies_bp.route('/enemies-test', methods=['GET'])
def test():
    return jsonify({'message': 'enemies'})


@enemies_bp.route('/enemies/generate', methods=['GET'])
def generate_enemies_route():
    #génère un nombre aléatoire d'énemies
    return generate_enemies()

@enemies_bp.route('/enemies', methods=['GET'])
def get_enemies():
    enemies = Enemy.query.all()
    result = []
    for enemy in enemies:
        result.append({
            'id': enemy.id,
            'name': enemy.name,
            'strength': enemy.strength,
            'health': enemy.health
        })
    return jsonify(result)

@enemies_bp.route('/enemies/<int:enemy_id>', methods=['GET'])
def get_enemy(enemy_id):
    enemy = db.session.get(Enemy, enemy_id)
    if enemy:
        enemy_data = {
            'id': enemy.id,
            'name': enemy.name,
            'strength': enemy.strength,
            'health': enemy.health
        }
        return jsonify(enemy_data)
    else:
        return jsonify({'message': 'Enemy not found'}), 404
