import unittest



# Models
from backend.tests.models.test_character import CharactersTestCase
from backend.tests.models.test_enemies import EnemiesTestCase
from backend.tests.models.test_events import EventsTestCase
from backend.tests.models.test_items import ItemsTestCase

# Routes
from backend.tests.routes.test_character import RoutesCharactersTestCase
from backend.tests.routes.test_enemies import RoutesEnemiesTestCase
from backend.tests.routes.test_events import RoutesEventsTestCase
from backend.tests.routes.test_items import RoutesItemsTestCase

# Controllers
from backend.tests.controllers.test_character import ControllerCharactersTestCase
from backend.tests.controllers.test_enemies import ControllerEnemiesTestCase

if __name__ == '__main__':
    unittest.main()